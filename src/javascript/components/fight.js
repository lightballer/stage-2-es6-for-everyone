import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const playerOneHealthBar = document.getElementById('left-fighter-indicator');
    const playerTwoHealthBar = document.getElementById('right-fighter-indicator');

    let playerOneHealth = firstFighter.health;
    let playerTwoHealth = secondFighter.health;
    
    let isPlayerOneInBlock = false;
    let isPlayerTwoInBlock = false;

    const critKeycodes = {};

    const deleteCritKey = event => {
      delete critKeycodes[event.code];
    };

    const endFight = (fighter, healthBar) => {
      healthBar.style.width = '0%';
      resolve(fighter);
      window.removeEventListener('keydown', fightControl, false);
      window.removeEventListener('keyup', deleteCritKey, false);
    };

    const changeHealthBar = (fighter, curHealth, healthBar) => {
      healthBar.style.width = (100 * curHealth / fighter.health) + '%';
    };
    
    const checkCritAvailability = critCombination => {
      const TEN_SEC = 10000;
      let lastCritTime = 0;
      return () => {
        let prevTime =  lastCritTime;
        const isCritCombinationPressed = !critCombination.map(keycode => critKeycodes[keycode] ? true : false).includes(false);
        const isTenSecondLeft = (new Date().getTime() - prevTime) >= TEN_SEC;
        const isCritAvailable = isCritCombinationPressed && isTenSecondLeft;
        if (isCritAvailable) {
          lastCritTime = new Date().getTime();
        }
        return isCritAvailable;
      }
    }
    
    const playerOneCrit = checkCritAvailability(controls['PlayerOneCriticalHitCombination']);
    const playerTwoCrit = checkCritAvailability(controls['PlayerTwoCriticalHitCombination']);
    
    const fightControl = event => {
      const keycode = event.code;
      critKeycodes[keycode] = true;

      if (keycode === controls['PlayerOneBlock']) {
        isPlayerOneInBlock = !isPlayerOneInBlock;
      } else if (keycode === controls['PlayerTwoBlock']) {
        isPlayerTwoInBlock = !isPlayerTwoInBlock;
      } else if (keycode === controls['PlayerOneAttack'] && !isPlayerOneInBlock) {  
        isPlayerTwoInBlock ?
          playerTwoHealth -= getDamage(firstFighter, secondFighter)
          : playerTwoHealth -= getHitPower(firstFighter);
        changeHealthBar(secondFighter, playerTwoHealth, playerTwoHealthBar);
      } else if (keycode === controls['PlayerTwoAttack'] && !isPlayerTwoInBlock) {
        isPlayerOneInBlock ?
          playerOneHealth -= getDamage(secondFighter, firstFighter)
          : playerOneHealth -= getHitPower(secondFighter);
        changeHealthBar(firstFighter, playerOneHealth, playerOneHealthBar);
      } else if (playerOneCrit()) {
        playerTwoHealth -= getHitPower(firstFighter) * 2;
        playerOneCrit.lastCritTime = new Date().getTime();
        changeHealthBar(secondFighter, playerTwoHealth, playerTwoHealthBar);
      } else if (playerTwoCrit()) {
        playerOneHealth -= getHitPower(secondFighter) * 2;
        playerTwoCrit.lastCritTime = new Date().getTime();
        changeHealthBar(firstFighter, playerOneHealth, playerOneHealthBar);
      }
      
      if (playerOneHealth <= 0) endFight(secondFighter, playerOneHealthBar);
      else if (playerTwoHealth <= 0) endFight(firstFighter, playerTwoHealthBar);
    };

    window.addEventListener('keydown', fightControl, false);
    window.addEventListener('keyup', deleteCritKey, false);
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const attack = getHitPower(attacker);
  const defense = getBlockPower(defender);  
  if (attack < defense) return 0;
  return attack - defense;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
