import { createElement } from '../../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function 
  const title = `${fighter.name} win!`;
  const bodyElement = createElement({tagName: 'p'});
  bodyElement.innerText = `Congratulations!`;
  showModal({ title, bodyElement });
}
