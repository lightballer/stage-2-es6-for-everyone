import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const imageClassName = position === 'right' ? 'mirror' : '';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter) {
    const { source, name, health, attack, defense } = fighter;
    const imageElement = createElement({
      tagName: 'img',
      className: imageClassName,
      attributes: {
        src: source,
      },
    });

    const infoElement = createElement({
      tagName: 'div',
      className: `fighter-preview___info`,
    });

    const nameElement = createElement({ tagName: 'h1' });
    nameElement.innerText = name;
    infoElement.append(nameElement);

    const healthElement = createElement({ tagName: 'p' });
    healthElement.innerText = `Health: ${health}`;
    infoElement.append(healthElement);

    const attackElement = createElement({ tagName: 'p' });
    attackElement.innerText = `Attack: ${attack}`;
    infoElement.append(attackElement);

    const defenseElement = createElement({ tagName: 'p' });
    defenseElement.innerText = `Defense: ${defense}`;
    infoElement.append(defenseElement);

    fighterElement.append(imageElement, infoElement);
  }
  
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
