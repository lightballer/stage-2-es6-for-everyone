import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    const endpoint = `../../../api/details/fighter/${id}.json`;
    const fighterData = await callApi(endpoint, 'GET');
    return fighterData;
  }
}

export const fighterService = new FighterService();
